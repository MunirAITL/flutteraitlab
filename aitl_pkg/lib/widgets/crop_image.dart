import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_crop/image_crop.dart';
import 'package:image_crop/image_crop.dart' as imgCrop;

class CropImagePage extends StatefulWidget {
  File file;
  String title;
  Color txtColor;
  Color appbarColor;
  CropImagePage({
    Key key,
    @required this.file,
    @required this.title,
    @required this.txtColor,
    @required this.appbarColor,
  }) : super(key: key);
  @override
  State createState() => _CropImagePageState();
}

class _CropImagePageState extends State<CropImagePage> {
  final cropKey = GlobalKey<CropState>();

  Future<void> _cropImage() async {
    final scale = cropKey.currentState.scale;
    final area = cropKey.currentState.area;
    if (area == null) {
      // cannot crop, widget is not setup
      return;
    }

    // scale up to use maximum possible number of pixels
    // this will sample image in higher resolution to make cropped image larger
    final sample = await ImageCrop.sampleImage(
      file: widget.file,
      preferredSize: (2000 / scale).round(),
    );

    final file = await ImageCrop.cropImage(
      file: sample,
      area: area,
    );

    sample.delete();

    //_lastCropped?.delete();
    //_lastCropped = file;

    debugPrint('$file');
    Get.back(result: file);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme:
              IconThemeData(color: widget.txtColor //change your color here
                  ),
          elevation: 0,
          backgroundColor: widget.appbarColor,
          title: FittedBox(
              fit: BoxFit.fitWidth,
              child: AutoSizeText(widget.title,
                  style: TextStyle(
                      color: widget.txtColor, fontWeight: FontWeight.bold))),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: widget.txtColor,
                  size: 25,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    double height = MediaQuery.of(context).size.height;
    return Stack(
      alignment: Alignment.center,
      children: [
        imgCrop.Crop.file(
          widget.file,
          key: cropKey,
          //alwaysShowGrid: true,
          aspectRatio: 1.5,
        ),
        Positioned(
          bottom: height * 12 / 100,
          child: Container(
            decoration: const BoxDecoration(
                shape: BoxShape.circle, color: Colors.green),
            child: IconButton(
                iconSize: 40,
                onPressed: () {
                  //EasyLoading.show();
                  _cropImage();
                },
                icon: const Icon(
                  Icons.check,
                  color: Colors.white,
                )),
          ),
        ),
      ],
    );
  }
}
/*class _CropImageDrvLicPageState extends State<CropImageDrvLicPage>
    with ScanMixin, Mixin {
  final _controller = CropController();

  Uint8List list8Data;
  int width, height;

  @override
  void initState() {
    super.initState();
    //  getting image data for cropping
    widget.file.readAsBytes().then((data) {
      if (mounted) {
        list8Data = data;
        setState(() {});
      }
    });
  }

  @override
  void dispose() {
    list8Data = null;
    super.dispose();
  }

  /*go2(File file2) {
    EasyLoading.dismiss();
    switch (widget.index) {
      case 0:
        Get.off(() => (widget.isFrontSide)
            ? DrivingFrontPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              )
            : DrivingBackPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              ));
        break;
      case 1:
        Get.off(() => (widget.isFrontSide)
            ? PassPortFrontPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              )
            : PassPortBackPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              ));
        break;
      case 2:
        Get.off(() => (widget.isFrontSide)
            ? EUIDCardFrontPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              )
            : EUIDCardBackPage(
                file: file2,
                index: widget.index,
                width: width,
                height: height,
              ));
        break;
      default:
    }
  }*/

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme:
              IconThemeData(color: MyTheme.brownColor //change your color here
                  ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: UIHelper().drawAppbarTitle(title: 'Crop your driving license'),
          centerTitle: false,
          actions: [
            (list8Data != null)
                ? IconButton(
                    onPressed: () {
                      scanDocData.file_drvlic_front = null;
                      Get.back(result: true);
                    },
                    icon: Icon(
                      Icons.close,
                      color: MyTheme.brownColor,
                      size: 25,
                    ))
                : SizedBox()
          ],
        ),
        body: (list8Data != null && mounted) ? drawLayout() : SizedBox(),
      ),
    );
  }

  drawLayout() {
    //final box = widget.rectKey.globalPaintBounds;
    //Offset position = box.localToGlobal(Offset.zero);
    return Stack(
      alignment: Alignment.center,
      children: [
        Crop(
            image: list8Data,
            aspectRatio: 1.5,
            //initialArea: box,
            //initialSize: .8,
            cornerDotBuilder: (size, cornerIndex) =>
                const DotControl(color: Colors.green),
            baseColor: MyTheme.themeData.accentColor.withOpacity(.5),
            maskColor: MyTheme.themeData.accentColor.withOpacity(.5),
            controller: _controller,
            onCropped: (_croppedData) async {
              // do something with image data
              //MemoryImage(_croppedData);
              if (_croppedData != null && mounted) {
                //final dir = await getExternalStorageDirectory();
                //final myImagePath = dir.path + "/myimg.png";
                //var f = File(myImagePath);
                //if (await f.exists()) {
                //await f.delete();
                //}
                /*_croppedData = resizeImage(
                  data: _croppedData,
                  width: ImageCfg.width,
                  height: ImageCfg.height,
                );*/
                //var decodedImage = await decodeImageFromList(_croppedData);
                //width = decodedImage.width;
                //height = decodedImage.height;
                final f = await widget.file.writeAsBytes(_croppedData);
                scanDocData.file_drvlic_front = f;
                EasyLoading.dismiss();
                Get.off(() => DrvLicReviewPage()).then((value) {
                  if (value != null) {
                    Get.off(() => OpenCamDrvLicPage());
                  }
                });
              }
            }),
        Positioned(
          top: getHP(context, 18),
          child: Container(
            width: getW(context),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Driving licence scanned",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 17,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: getHP(context, 15),
          child: Container(
            decoration:
                BoxDecoration(shape: BoxShape.circle, color: Colors.green),
            child: IconButton(
                iconSize: 40,
                onPressed: () {
                  EasyLoading.show();
                  _controller.crop();
                },
                icon: Icon(
                  Icons.check,
                  color: Colors.white,
                )),
          ),
        ),
      ],
    );
  }
}*/
