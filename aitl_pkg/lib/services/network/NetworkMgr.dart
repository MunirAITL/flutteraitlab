//  https://github.com/flutterchina/dio
// ignore_for_file: file_names

import 'dart:convert';
import 'dart:io';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart' as g;
import 'package:http/http.dart' show get;
import 'package:http_parser/http_parser.dart';
import 'package:json_string/json_string.dart';
import 'package:mime_type/mime_type.dart';
import 'package:path_provider/path_provider.dart';
import 'CookieMgr.dart';

//typedef mapValue = Function(Map<String, dynamic>);
/*
enum NetEnum { Available, NotAvailable, ServerDown }

enum ReqType {
  Get,
  Post,
  Put,
  Delete,
  Head,
  Patch,
  Copy,
  Options,
  Link,
  UnLink,
  Purge,
  Lock,
  UnLock,
  PropFind,
  View,
}

class NetworkMgr2 {
  static final NetworkMgr2 shared = NetworkMgr2._internal();
  factory NetworkMgr2() {
    return shared;
  }
  NetworkMgr2._internal();

  Future<dynamic> req(
      {context,
      url,
      param,
      reqType = ReqType.Post,
      isLoading = true,
      isCookie = false}) async {
    final NetEnum netEnum = await hasNetwork();
    if (netEnum == NetEnum.Available) {
      try {
        if (isLoading) {
          NetworkHelper().startLoading();
        }
        NetworkHelper()
            .log("WS::" + reqType.toString() + "==================" + url);
        try {
          for (var p in param.entries) {
            NetworkHelper().log(p.key + '=' + p.value.toString());
          }
        } catch (e) {}

        var _dio = Dio();
        _dio.options.headers = CookieMgr2.headers;

        //if (isCookie) {
        CookieJar cj = await CookieMgr2().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        if (isCookie) {
          cj.saveFromResponse(
              Uri.parse(url), await cj.loadForRequest(Uri.parse(url)));
          NetworkHelper().log(cj.loadForRequest(Uri.parse(url)));
        }

        var response;
        if (reqType == ReqType.Post)
          response = await _dio.post(url, data: param);
        else if (reqType == ReqType.Put)
          response = await _dio.put(url, data: param);
        else if (reqType == ReqType.Delete)
          response = await _dio.delete(url);
        else if (reqType == ReqType.Get)
          response = await _dio.get(url, queryParameters: param ?? {});
        else if (reqType == ReqType.Head)
          response = await _dio.head(url);
        else if (reqType == ReqType.Patch) response = await _dio.patch(url);
        _dio.close();

        final jsonString = JsonString(json.encode(response.data));
        NetworkHelper().log(jsonString.source);
        if (isLoading) {
          NetworkHelper().stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          return response.data;
        } else {
          return null;
        }
      } catch (e) {
        NetworkHelper().log(
            "******************** NETWORK ERROR *****************************");
        NetworkHelper().log(e.toString());
        NetworkHelper().log(
            "***************************************************************");
        if (isLoading) {
          NetworkHelper().stopLoading();
        }
      }
    } else if (netEnum == NetEnum.NotAvailable) {
      NetworkHelper().showSnake(
          "Alert!", "No internet connection. Please try again later");
    } else if (netEnum == NetEnum.ServerDown) {
      NetworkHelper().showSnake(
          "Alert!", "No internet connection. Please try again later");
    }
    return null;
  }

  //  files only
  Future<dynamic> uploadFiles(
      {context, url, List<File> files, isLoading = true}) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          NetworkHelper().startLoading();
        }
        NetworkHelper()
            .log("ws::uploadFiles:: files only  ==================" + url);
        var formData = FormData();
        for (var file in files) {
          //final String mimeStr = lookupMimeType(file.path);
          //var fileType = mimeStr.split('/');
          //log(fileType[0]);
          //log('file type ${mimeStr}, ${fileType}');
          //final String fileName = file.path.split('/').last;
          //log(MediaType(fileType[0], mimeStr));
          /*formData.files.addAll(
            [
              MapEntry(
                  "file",
                  await MultipartFile.fromFile(file.path,
                      filename: basename(file.path),
                      contentType: MediaType(fileType[0], mimeStr)))
            ],
          );*/

          String mimeType = mime(file.path);
          String mimee = mimeType.split('/')[0];
          String type = mimeType.split('/')[1];

          formData = FormData.fromMap({
            "files": [
              await MultipartFile.fromFile(
                file.path,
                filename: file.path,
                contentType: MediaType(mimee, type),
              )
            ],
          });
        }

        var _dio = Dio();
        _dio.options.headers = {
          'Content-type': 'multipart/form-data',
        };
        CookieJar cj = await CookieMgr2().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(
            Uri.parse(url), await cj.loadForRequest(Uri.parse(url)));
        NetworkHelper().log(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(url, data: formData);
        _dio.close();
        NetworkHelper().log(response.data.toString());
        if (isLoading) {
          NetworkHelper().stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          return response.data;
        } else {
          return null;
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        NetworkHelper().showSnake(
            "Alert!", "No internet connection. Please try again later");
      } else if (netEnum == NetEnum.ServerDown) {
        NetworkHelper().showSnake(
            "Alert!", "No internet connection. Please try again later");
      }
    } catch (e) {
      NetworkHelper().log(
          "******************** NETWORK ERROR *****************************");
      NetworkHelper().log(e.toString());
      NetworkHelper().log(
          "***************************************************************");
      if (isLoading) {
        NetworkHelper().stopLoading();
      }
    }
  }

  //  multipart
  Future<dynamic> postMultiPart({context, url, param, isLoading = true}) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          NetworkHelper().startLoading();
        }
        NetworkHelper()
            .log("ws::postFile :: multipart  ==================" + url);
        var _dio = Dio();
        _dio.options.headers = CookieMgr2.headers;
        CookieJar cj = await CookieMgr2().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(
            Uri.parse(url), await cj.loadForRequest(Uri.parse(url)));
        //log(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(url, data: FormData.fromMap(param));
        _dio.close();
        NetworkHelper().log(response.data.toString());
        if (isLoading) {
          NetworkHelper().stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          return response.data;
        } else {
          return null;
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        NetworkHelper().showSnake(
            "Alert!", "No internet connection. Please try again later");
      } else if (netEnum == NetEnum.ServerDown) {
        NetworkHelper().showSnake(
            "Alert!", "No internet connection. Please try again later");
      }
    } catch (e) {
      NetworkHelper().log(
          "******************** NETWORK ERROR *****************************");
      NetworkHelper().log(e.toString());
      NetworkHelper().log(
          "***************************************************************");
      if (isLoading) {
        NetworkHelper().stopLoading();
      }
    }
  }

  downloadFile({
    BuildContext context,
    String url,
    String pathName,
    isLoading = true,
    Function callback,
  }) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          NetworkHelper().startLoading();
        }
        var response = await get(Uri.parse(url));
        if (isLoading) {
          NetworkHelper().stopLoading();
        }
        if (response.statusCode == 200) {
          NetworkHelper().log("ws::downloadFile ==================" + url);
          final String fileName = url.split('/').last;
          var tempDir = await getTemporaryDirectory();
          String fullPath = tempDir.path + '/' + fileName;

          File file = File(fullPath);
          var raf = file.openSync(mode: FileMode.write);
          raf.writeFromSync(response.bodyBytes);
          await raf.close();
          callback(fullPath);
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        NetworkHelper().showSnake(
            "Alert!", "No internet connection. Please try again later");
      } else if (netEnum == NetEnum.ServerDown) {
        NetworkHelper().showSnake(
            "Alert!", "Something went wrong. Please try again later");
      }
    } catch (e) {
      NetworkHelper().log(
          "******************** NETWORK ERROR *****************************");
      NetworkHelper().log(e.toString());
      NetworkHelper().log(
          "***************************************************************");
      if (isLoading) {
        NetworkHelper().stopLoading();
      }
    }
  }

  dispose() {
    try {
      NetworkHelper().stopLoading();
      //_dio.close();
      //_dio = null;
    } catch (e) {}
  }

  //  0=ok
  //  1=internet not available
  //  2=server is down
  Future<NetEnum> hasNetwork() async {
    return (await DataConnectionChecker().hasConnection)
        ? NetEnum.Available
        : NetEnum.NotAvailable;
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class NetworkHelper {
  showSnake(title, msg) {
    g.Get.snackbar(title, msg,
        backgroundColor: Colors.white, colorText: Colors.black);
  }

  void startLoading() {
    EasyLoading.show(status: "Loading...");
  }

  void stopLoading() {
    EasyLoading.dismiss();
  }

  log(str) {
    if (const String.fromEnvironment('DEBUG') != null) {
      debugPrint(str);
      //final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      //pattern.allMatches(str).forEach((match) => print(match.group(0)));
    }
  }
}
*/