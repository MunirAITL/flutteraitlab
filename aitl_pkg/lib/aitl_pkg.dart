library aitl_pkg;

/// A Calculator.
class munirAitl {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 1;
}
